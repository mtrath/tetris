#pragma once

#include <SDL.h>

namespace sdl
{
    namespace rgb
    {

        /* red component of rgb */
        struct R {
            constexpr R(Uint8 r)
                : m_value(r) {}

            constexpr explicit operator Uint8()
            {
                return m_value;
            }

            constexpr auto toUint8() const
            {
                return m_value;
            }

          private:
            Uint8 m_value;
        };

        /* green component of rgb */
        struct G {
            constexpr G(Uint8 g)
                : m_value(g)
            {
            }

            constexpr explicit operator Uint8()
            {
                return m_value;
            }

            constexpr auto toUint8() const
            {
                return m_value;
            }

          private:
            Uint8 m_value;
        };

        /* blue component of rgb */
        struct B {
            constexpr B(Uint8 b)
                : m_value(b)
            {
            }

            constexpr explicit operator Uint8()
            {
                return m_value;
            }

            constexpr auto toUint8() const
            {
                return m_value;
            }

          private:
            Uint8 m_value;
        };

        /* alpha component of rgba
         *
         * 0xff -> opaque
         * 0x00 -> transparent
         */
        struct A {
            constexpr A(Uint8 a)
                : m_value(a)
            {
            }

            constexpr explicit operator Uint8()
            {
                return m_value;
            }

            constexpr auto toUint8() const
            {
                return m_value;
            }

          private:
            Uint8 m_value;
        };

        /* RGB(0xff_R, 0xff_G, 0xff_B) -> white
         * RGB(0x00_R, 0x00_G, 0x00_B) -> black
         */
        struct RGB {
            constexpr RGB(R r, G g, B b)
                : m_r(r), m_g(g), m_b(b)
            {
            }

            auto toUint32(const SDL_PixelFormat* format) const
            {
                Expects(format);
                return SDL_MapRGB(
                    format,
                    m_r.toUint8(),
                    m_g.toUint8(),
                    m_b.toUint8());
            }

          private:
            const R m_r;
            const G m_g;
            const B m_b;
        };

        /* RGBA(0xff_R, 0xff_G, 0xff_B, 0xff_A) -> white
         * RGBA(0x00_R, 0x00_G, 0x00_B, 0xff_A) -> black
         */
        struct RGBA {
            constexpr RGBA(R r, G g, B b, A a)
                : m_r(r), m_g(g), m_b(b), m_a(a)
            {
            }

            auto toUint32(const SDL_PixelFormat* format) const
            {
                Expects(format);
                return SDL_MapRGBA(
                    format,
                    m_r.toUint8(),
                    m_g.toUint8(),
                    m_b.toUint8(),
                    m_a.toUint8());
            }

          private:
            const R m_r;
            const G m_g;
            const B m_b;
            const A m_a;
        };

        /* User defined literals to create R, G, B and A instances from
         * integer literals.  Expected range is [0..255].
         *
         * Declare 'using namespace sdl::rgb::literals' to use them.
         *
         * Examples:
         *  - auto red = 100_R;
         *  - auto color = RGB(0x10_R, 112_G, 022_B);
         */
        inline namespace literals
        {

            /* red */
            constexpr auto operator"" _R(unsigned long long r) -> R
            {
                Expects(r < 256);
                return R(r);
            }

            /* green */
            constexpr auto operator"" _G(unsigned long long g) -> G
            {
                Expects(g < 256);
                return G(g);
            }

            /* blue */
            constexpr auto operator"" _B(unsigned long long b) -> B
            {
                Expects(b < 256);
                return B(b);
            }

            /* alpha */
            constexpr auto operator"" _A(unsigned long long a) -> A
            {
                Expects(a < 256);
                return A(a);
            }

        } // namespace literals

    } // namespace rgb
} // namespace sdl
