#pragma once

#include <SDL.h>
#include <gsl/gsl>

#ifdef DEBUG
#include <iostream>
#endif

namespace sdl
{

    class Sdl final
    {
      public:
        Sdl()
        {
            auto result = SDL_Init(SDL_INIT_VIDEO);
            Expects(result >= 0);
        }
        Sdl(const Sdl&) = delete;
        Sdl& operator=(const Sdl&) = delete;
        Sdl(Sdl&&) = default;
        Sdl& operator=(Sdl&&) = default;

        ~Sdl()
        {
#ifdef DEBUG
            std::cout << "dtor sdl" << std::endl;
#endif
            SDL_Quit();
        }
    };

} // namespace sdl
