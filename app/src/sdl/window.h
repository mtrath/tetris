#pragma once

#include <SDL.h>
#include <gsl/gsl>

#include "surface.h"

#ifdef DEBUG
#include <iostream>
#endif

namespace sdl
{

    class Sdl;

    class Window final
    {
      public:
        enum class Flags {
            Shown = SDL_WINDOW_SHOWN
        };

        static const int k_PosUndefined = SDL_WINDOWPOS_UNDEFINED;
        static const int k_PosCentered = SDL_WINDOWPOS_CENTERED;

        Window(std::shared_ptr<Sdl> sdl, const char* title, int x, int y, int w, int h, Flags flags)
            : m_sdl(sdl), m_window(SDL_CreateWindow(title, x, y, w, h, static_cast<Uint32>(flags)), SDL_DestroyWindow)
        {
            Expects(m_window);
        }
        Window(const Window&) = delete;
        Window& operator=(const Window&) = delete;
        Window(Window&&) = default;
        Window& operator=(Window&&) = default;

#ifdef DEBUG
        ~Window()
        {
            std::cout << "dtor window" << std::endl;
        }
#endif

        auto getSurface() const -> Surface
        {
            return Surface(SDL_GetWindowSurface(m_window.get()));
        }

        auto updateSurface() const
        {
            SDL_UpdateWindowSurface(m_window.get());
        }

      private:
        std::shared_ptr<Sdl> m_sdl{nullptr};
        std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> m_window{nullptr, nullptr};
    };

} // namespace sdl
