#pragma once

#include <SDL.h>
#include <gsl/gsl>

#include "sdl/utils.h"

#ifdef DEBUG
#include <iostream>
#endif

namespace sdl
{

    class Surface final
    {
        friend class Window;

      public:
        Surface(const Surface&) = delete;
        Surface& operator=(const Surface&) = delete;
        Surface(Surface&& surface) = default;
        Surface& operator=(Surface&& surface) = default;

#ifdef DEBUG
        ~Surface()
        {
            std::cout << "dtor surface" << std::endl;
        }
#endif

        void fillRect(const SDL_Rect* rect, sdl::rgb::RGB color)
        {
            if (SDL_FillRect(m_surface, rect, color.toUint32(m_surface->format)) == -1) {
                std::runtime_error ex("error occured during fillRect RGB");
            }
        }

        void fillRect(const SDL_Rect* rect, sdl::rgb::RGBA color)
        {
            if (SDL_FillRect(m_surface, rect, color.toUint32(m_surface->format)) == -1) {
                throw std::runtime_error("error occured during fillRect RGBA");
            }
        }

      private:
        explicit Surface(SDL_Surface* surface)
            : m_surface(surface)
        {
        }

        SDL_Surface* m_surface{nullptr};
    };

} // namespace sdl
