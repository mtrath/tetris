#include <gsl/gsl>
#include <iostream>
#include <memory>

#include "sdl/sdl.h"
#include "sdl/utils.h"
#include "sdl/window.h"

bool loadMedia();
void close();

int main()
{
    constexpr int k_ScreenWidth = 640;
    constexpr int k_ScreenHeight = 480;

    using namespace sdl::rgb::literals;
    constexpr sdl::rgb::RGB background(0xff_R, 0xff_G, 0xff_B);

    auto sdl = std::make_shared<sdl::Sdl>();
    sdl::Window window(sdl, "SDL Tutorial",
                       sdl::Window::k_PosUndefined, sdl::Window::k_PosUndefined,
                       k_ScreenWidth, k_ScreenHeight,
                       sdl::Window::Flags::Shown);

    auto surface = window.getSurface();
    surface.fillRect(nullptr, background);
    window.updateSurface();

    SDL_Delay(2000);

    return 0;
}
