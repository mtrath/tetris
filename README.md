# Tetris

Tetris is just a simple project to wrap the [SDL](https://www.libsdl.org)
library using C++14. The C++ SDL wrapper might be released as a separate
project later.


## License

See the [LICENSE](LICENSE.md) file for license rights and limitations (3-Clause
BSD).
